Conda dartlang package
======================

Conda package for the Dart language SDK. Useful for running dart2js on
*Read the Docs* (among other things). Installs the SDK to the root of the
conda environment so that the ``dart``, ``dart2js``, ``pub`` ... commands
are directly in the PATH. This means that a file called ``version`` ends
up in the environment prefix folder (``pub`` requires this file to work).

To build the package run::

    conda build .

To upload to anaconda.org::

    conda install anaconda-client
    source activate
    anaconda upload FILE_NAME.tar.bz2

To install the package in a conda env::

    conda install -c trlandet dartlang

This recipe currently only works for Linux on x86_64.

Conda recipe by Tormod Landet, 2018. The Dart SDK is licensed under the
modified BSD license, see the `LICENSE file`_ in the Dart repo.

.. _LICENSE file: https://github.com/dart-lang/sdk/blob/b33dcfdaad5ff4e37ea8a4b5815ca3769813d656/LICENSE

